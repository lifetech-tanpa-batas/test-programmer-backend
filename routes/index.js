const router = require("express").Router();

const data = [];
for (let i = 1; i < 100; i++) {
    data.push({ id: i, username: `user${i}`, email: `email${i}@m.huda.n` });
}

router.get("/api/v1", (req, res) => {
    res.json(data);
});

router.post("/api/v1", (req, res) => {
    const newUser = req.body;
    data.push(newUser);
    res.json({ message: "Data berhasil ditambahkan", data: newUser });
});

router.put("/api/v1/:id", (req, res) => {
    const userId = parseInt(req.params.id);
    const updatedUser = req.body;
    const index = data.findIndex(user => user.id === userId);

    if (index !== -1) {
        data[index] = { ...data[index], ...updatedUser };
        res.json({ message: "Data berhasil diubah", data: data[index] });
    } else {
        res.status(404).json({ message: "Data tidak ditemukan" });
    }
});

router.delete("/api/v1/:id", (req, res) => {
    const userId = parseInt(req.params.id);
    const index = data.findIndex(user => user.id === userId);

    if (index !== -1) {
        const deletedUser = data.splice(index, 1);
        res.json({ message: "Data berhasil dihapus", data: deletedUser });
    } else {
        res.status(404).json({ message: "Data tidak ditemukan" });
    }
});

module.exports = router;
