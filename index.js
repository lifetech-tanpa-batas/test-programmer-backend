const express = require('express');
const routes = require("./routes");
const cors = require("cors");
const app = express();
const PORT = 4000;

app.use(express.json());
app.use(cors({ origin: "*", optionsSuccessStatus: 200 }));

app.use(routes);

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});